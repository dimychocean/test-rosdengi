### Installation
``` bash
$ git clone https://dimychocean@bitbucket.org/dimychocean/test-rosdengi.git
$ cd test-rosdengi
$ npm i
```

### Run
``` bash
$ npm start
```

This will start a server listening on port ```3000```.
*You can change the port in* ```app.js``` *or by setting the ```PORT``` environment variable*.
