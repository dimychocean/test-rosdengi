exports.index = (req, res) => {
    const data = {
        message: `Welcome to test app!`
    };

    res.render('index', Object.assign(data, req.commonData));
};

exports.error404 = (req, res) => res.sendStatus(404);
