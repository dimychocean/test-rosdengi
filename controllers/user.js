const User = require('../models/user');

exports.login = (req, res) => {
    const data = {};

    if (req.session.authFailMessage) {
        data.error = req.session.authFailMessage;
    }

    res.render('auth', Object.assign(data, req.commonData));
};

exports.profile = (req, res) => {
    const data = {};

    data.user = req.user;

    res.render('profile', Object.assign(data, req.commonData));
};

exports.friends = (req, res) => {
    const data = {};

    data.friends = req.user.friends;
    data.inboxFriends = req.user.inbox;
    data.outboxFriends = req.user.outbox;

    res.render('friends', Object.assign(data, req.commonData));
};

exports.search = (req, res) => {
    const data = {};

    data.users = User.getUsers();

    res.render('search', Object.assign(data, req.commonData));
};

exports.item = (req, res) => {
    const id = parseInt(req.params.id);
    const user = User.findById(id);

    if (!user) {
        res.sendStatus(404);
        return;
    }

    const data = {};
    data.user = user;

    res.render('profile', Object.assign(data, req.commonData));
};