'use strict'

const express = require('express');
const path = require('path');
const app = express();
const bodyParser = require('body-parser');

const passport = require('./lib/passport');

app.set('port', (process.env.PORT || 3000));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'jade'));
app.set('view engine', 'jade');

app.use(require('express-session')({
    secret: 'rosdengitestapp',
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initUser);

require('./routes')(app);

app.listen(app.get('port'), () => console.log(`Listening on port ${app.get('port')}`));

module.exports = app;
