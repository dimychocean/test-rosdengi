'use strict';

const crypto = require('crypto');

const users = [
    {id: 1,
     username: 'Stepan Spepanov',
     login: 'admin',
     hash: '0192023a7bbd73250516f069df18b500',
     salt: '123',
     friends: [
        {friendId: 2,
         username: 'Ivan Ivanov'},
        {friendId: 3,
         username: 'Dimych Ocean'}
     ],
     inbox: [],
     outbox: []
    },
    {id: 2,
     username: 'Ivan Ivanov',
     login: 'user',
     hash: 'f67c683f0f3e98cb9dd5582e8cbbcd04',
     salt: '321',
     friends: [
        {friendId: 1,
         username: 'Stepan Spepanov'}
     ],
     inbox: [],
     outbox: [
        {friendId: 3,
         username: 'Dimych Ocean'}
    ]
    },
    {id: 3,
     username: 'Dimych Ocean',
     login: 'ocean',
     hash: 'f67c683f0f3e98cb9dd5582e8cbbcd04',
     salt: '321',
     friends: [
        {friendId: 1,
         username: 'Stepan Spepanov'}
     ],
     inbox: [
        {friendId: 2,
         username: 'Ivan Ivanov'}
     ],
     outbox: []
    }
];

function encryptPassword(password, salt) {
    return crypto.createHash('md5').update(password + salt).digest('hex');
}

class User {
    constructor(props) {
        this.id = props.id;
        this.username = props.username;
        this.login = props.login;

        this.hash = props.hash;
        this.salt = props.salt;

        this.friends = props.friends;
        this.inbox = props.inbox;
        this.outbox = props.outbox;
    }

    checkPassword(password) {
        const hash = encryptPassword(password, this.salt);

        return this.hash === hash;
    }

    static findByName(username) {
        var user = users.find(user => user.username === username);

        return user && new User(user);
    }

    static findByLogin(login) {
        var user = users.find(user => user.login === login);

        return user && new User(user);
    }

    static findById(id) {
        var user = users.find(user => user.id === id);

        return user && new User(user);
    }

    static getSerializator() {
        return user => user.id;
    }

    static getDeserializator() {
        return id => User.findById(id);
    }

    static getUsers() {
        return users;
    }
}

module.exports = User;
