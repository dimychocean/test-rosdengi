'use strict';

var BaseStrategy = require('./base');

class LocalStrategy extends BaseStrategy {
    authenticate(req, done) {
        const username = req.body.username;
        const login = req.body.login;
        const password = req.body.password;

        this._verify(login, password, done);
    }
}

module.exports = LocalStrategy;
