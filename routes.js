'use strict'

const user = require('./controllers/user');
const pages = require('./controllers/pages');
const passport = require('./lib/passport');

module.exports = (app) => {
	app.get('/', pages.index);

	app.get('/auth', user.login);
    app.post('/auth', passport.authenticate('local', {
        successRedirect: '/friends',
        failureRedirect: '/auth'
    }));
	
	app.get('/profile', passport.onlyAuth, user.profile);

	app.get('/profile/:id', passport.onlyAuth, user.item);

	app.get('/friends', passport.onlyAuth, user.friends);
	
	app.get('/search', passport.onlyAuth, user.search);

	app.all('*', pages.error404);
	
	app.use((err, req, res, next) => {
        console.error(err);

        res.sendStatus(500);
    });
};